# jd_auth

On the way:
- More config changes.
- A LOT of Code/Syntax cleanups.
- An actual proper Readme file :).

For the examples, I auto load the following:
- $autoload['libraries'] = array('database', 'session', 'phpass');
- $autoload['helper'] = array('url');

I make use of the following js plugin:
https://github.com/zuzara/jQuery-OAuth-Popup (few changes made to center the popup window)
