<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

//Database settings
$config["users_table"] = "users";
$config["user_groups_table"] = "user_groups";
$config["users_groups_table"] = "users_groups";

//Default group settings
$config["default_group"] = "member";
$config["admin_group"] = "admin";

//Login Settings
$config["login_credential"] = "username"; //username or email

//Remember me cookie Settings
$config["cookie_id_column"] = "id"; //the column on the database to check the cookie against - MUST BE A UNIQUE DB VALUE!!!
$config["cookie_id_name"] = "Yoursite_" . $config["cookie_id_column"];
$config["cookie_remember_code_name"] = "Yoursite_remember_code";

//Email settings
$config["site_title"] = "Yoursite.com";
$config["email"] = "noreply@Yoursite.com";
$config["email_from"] = "Yoursite.com";

?>
