<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library("jd_auth");
    }

    public function index() {
        if ($this->jd_auth->logged_in()) {
            echo "<br>Logged in! Now <a href='".base_url()."index.php/logout'>logout</a>";
            exit();
        }

        $this->load->helper("form");
        $this->load->library("form_validation");
        $this->load->library("fblogin");

        $this->form_validation->set_rules("username", "Username", "required");
        $this->form_validation->set_rules("password", "Password", "required");

        if ($this->form_validation->run() == FALSE) {
            $data["main_content"] = "auth/login";
            $this->load->view("inc/template", $data);
        } else {
            $this->load->model("auth_model");
            $this->load->library("phpass");
            $username = $this->input->post("username");
            $password = $this->input->post("password");
            $user = $this->auth_model->check_login($username, $password);

            if ($user) {
                if ($user->active == 0) {
                    $data["credential_error"] = "Account is not active yet! Check your email.";
                    $data["main_content"] = "auth/login";
                    $this->load->view("inc/template", $data);
                } else {
                    $this->auth_model->set_user_session($user);

                    if ($this->input->post("remember_me")) {
                        $this->auth_model->remember_me($user);
                    }

                    $this->auth_model->update_last_login($user->id);
                   echo "<br>Logged in! Now <a href='".base_url()."index.php/logout'>logout</a>";
                }
            } else {
                $data["credential_error"] = "Wrong Username and/or Password.";
                $data["main_content"] = "auth/login";
                $this->load->view("inc/template", $data);
            }
        }
    }

    //FACEBOOK LOGIN
    public function facebook_login() {
        $this->load->library("fblogin");

        $data = array(
            "redirect_uri" => site_url("auth/handle_facebook_login"),
            "display" => "popup",
            "scope" => "email"
        );

        redirect($this->fblogin->getLoginUrl($data));
    }

    public function handle_facebook_login() {
        $this->load->library("fblogin");
        $this->load->model("auth_model");
        $facebook_user = $this->fblogin->user;

        if ($this->fblogin->user) {
            if ($this->auth_model->is_member_fb($facebook_user)) {
                $this->auth_model->login_fb($facebook_user);
            } else {
                $this->auth_model->signup_from_facebook($facebook_user);
                $this->auth_model->login_fb($facebook_user);
            }
            echo '<script type="text/javascript">window.close();</script>';
        } else {
            echo '<script type="text/javascript">window.close();</script>';
            echo "Could not login at this time";
        }
    }

    //END FACEBOOK LOGIN

    function register() {
        $this->load->helper("form");
        $this->load->library("form_validation");

        $this->form_validation->set_rules("username", "Username", "trim|min_length[3]|max_length[20]|required|callback_regex_check|callback_username_check");
        $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[20]|matches[password_conf]");
        $this->form_validation->set_rules("password_conf", "Password Confirmation", "trim");
        $this->form_validation->set_rules("email", "Email", "trim|required|valid_email|is_unique[users.email]");

        if ($this->form_validation->run() == FALSE) {
            $data["username_tooltip"] = array(
                "name" => "username",
                "id" => "username",
                "class" => "tooltip",
                "title" => "Characters Permitted: A to Z, 0 to 9, _ and -",
            );
            $data["password_tooltip"] = array(
                "name" => "password",
                "id" => "password",
                "class" => "tooltip",
                "title" => "Passwords must be at least 6 characters long",
                "autocomplete" => "off",
            );
            $data["password_conf"] = array(
                "name" => "password_conf",
                "id" => "password_conf",
                "autocomplete" => "off",
            );
            $data["email_tooltip"] = array(
                "name" => "email",
                "id" => "email",
                "class" => "tooltip",
                "title" => "A valid email is necessary to finish the registration process",
            );
            $data["main_content"] = "auth/register";
            $this->load->view("inc/template", $data);
        } else {
            $this->load->library("phpass");

            $username = $this->input->post("username");
            $email = $this->input->post("email");
            $hashed_password = $this->phpass->hash($this->input->post("password"));
            $register_key = str_shuffle(sha1($hashed_password));

            $this->jd_auth->create_user($username, $email, $hashed_password, $register_key);
            $this->session->set_flashdata("message", "Successfully registered! Check email to activate account.");
            redirect("login");
        }
    }

    public function activate($id, $str) {
        if ($str) {
            $is_activated = $this->auth_model->activate($id, $str);

            if ($is_activated) {
                $this->session->set_flashdata("message", "Account activated successfully! You may now login.");
                redirect("login");
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function password_reset($id, $str) {
        if ($str) {
            $user = $this->auth_model->check_password_reset_hash($id, $str);

            if ($user) {
                $this->load->helper("form");
                $this->load->library("form_validation");

                $this->form_validation->set_rules("password", "Password", "required|min_length[8]|matches[password_confirm]");
                $this->form_validation->set_rules("password_confirm", "Password Confirm", "required");

                if ($this->form_validation->run() == false) {
                    $data["password"] = array(
                        "name" => "password",
                        "id" => "password",
                        "type" => "password"
                    );

                    $data["password_confirm"] = array(
                        "name" => "password_confirm",
                        "id" => "password_confirm",
                        "type" => "password"
                    );

                    $data["hash"] = $user->lost_password_key;
                    $data["user_id"] = $user->id;

                    $data["main_content"] = "auth/password_reset";
                    $this->load->view("inc/template", $data);
                } else {
                    $this->load->library("phpass");
                    $hashed_password = $this->phpass->hash($this->input->post("password"));
                    $this->auth_model->change_password($user->id, $hashed_password);
                    $this->session->set_flashdata("message", "Password successfully reseted! You may now login.");
                    redirect("login");
                }
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    public function lost_password() {
        $this->load->helper("form");
        $this->load->library("form_validation");

        $this->form_validation->set_rules("email", "Email", "trim|required|valid_email|callback_check_lost_email");

        if ($this->form_validation->run() == FALSE) {
            $data["email"] = array(
                "name" => "email",
                "id" => "email",
            );
            $data["main_content"] = "auth/lost_password";
            $this->load->view("inc/template", $data);
        } else {
            $email = $this->input->post("email");

            $user_info = $this->auth_model->set_lost_password_hash($email);

            $this->jd_auth->lost_password_email($email, $user_info["user_id"], $user_info["username"], $user_info["lost_password_code"]);

            $this->session->set_flashdata("message", "Instructions to reset password sent to Email.");
            redirect("login");
        }
    }

    public function logout() {
        $this->jd_auth->logout();
    }

    function regex_check($str) {
        if (preg_match("/^[a-zA-Z0-9_-]*$/", $str)) {
            return TRUE;
        } else {
            $this->form_validation->set_message("regex_check", "Invalid characters in Username.");
            return FALSE;
        }
    }

    function username_check($str) {
        $this->load->model("auth_model");

        if ($this->auth_model->username_check($str) > 0) {
            $this->form_validation->set_message("username_check", "This %s already exists.");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function email_check($str) {
        $this->load->model("auth_model");

        if ($this->auth_model->email_check($str) > 0) {
            $this->form_validation->set_message("email_check", "This %s is already in the database.");
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_lost_email($str) {
        $this->load->model("auth_model");

        if ($this->auth_model->email_check($str) < 1) {
            $this->form_validation->set_message("check_lost_email", "This %s is not in the database.");
            return FALSE;
        } else {
            return TRUE;
        }
    }
}

?>
