<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Jd_auth {

    public function __construct() {
        $this->config->load("jd_auth_config", TRUE);
        $this->load->model("auth_model");
        $this->load->helper("cookie");
        $cookie_id_name = $this->config->item("cookie_id_name", "jd_auth_config");
        $cookie_remember_code_name = $this->config->item("cookie_remember_code_name", "jd_auth_config");

        //check to see if user is active - stop access to website if not.
        if ($this->logged_in()) {
            $user = $this->user();
            if ($user) {
                if ($user->active != 1) {
                    $this->logout();
                }
            } else {
                $this->logout();
            }
        }

        //check for remember me cookies, log user if those are set.
        if (!$this->logged_in() && get_cookie($cookie_id_name) && get_cookie($cookie_remember_code_name)) {
            $this->auth_model->login_remembered();
        }
    }

    //no need to declare extra variable to use CI super-global
    public function __get($var) {
        return get_instance()->$var;
    }

    public function create_user($username, $email, $hashed_password, $register_key) {
        $new_user = array(
            "username" => $username,
            "password" => $hashed_password,
            "email" => $email,
            "creation_date" => time(),
            "register_key" => $register_key
        );

        $user_id = $this->auth_model->create_user($new_user);

        //insert user, insert into default group and send register_key email

        $data = array(
            "title" => $this->config->item("site_title", "jd_auth_config"),
            "username" => $username,
            "register_key" => $register_key,
            "user_id" => $user_id
        );

        $message = $this->load->view("email/register", $data, true);
        $this->load->library("email");
        $this->email->set_mailtype("html");

        $this->email->from($this->config->item("email", "jd_auth_config"), $this->config->item("email_from", "jd_auth_config"));
        $this->email->to($email);

        $this->email->subject("Welcome to " . $this->config->item("site_title", "jd_auth_config"));
        $this->email->message($message);

        $this->email->send();
    }

    public function lost_password_email($email, $user_id, $username, $lost_password_code) {
        $data = array(
            "title" => $this->config->item("site_title", "jd_auth_config"),
            "user_id" => $user_id,
            "username" => $username,
            "lost_password_code" => $lost_password_code
        );

        $message = $this->load->view("email/lost_password", $data, true);
        $this->load->library("email");
        $this->email->set_mailtype("html");

        $this->email->from($this->config->item("email", "jd_auth_config"), $this->config->item("email_from", "jd_auth_config"));
        $this->email->to($email);

        $this->email->subject($this->config->item("site_title", "jd_auth_config") . " - Password Reset");
        $this->email->message($message);

        $this->email->send();
    }

    public function logout() {
        $this->session->unset_userdata("username");
        $this->session->unset_userdata("email");
        $this->session->unset_userdata("fb_login");
        $this->session->unset_userdata("user_id");
        $this->session->unset_userdata("is_active");
        $this->session->sess_destroy();

        if (get_cookie($this->config->item("cookie_id_name", "jd_auth_config"))) {
            delete_cookie($this->config->item("cookie_id_name", "jd_auth_config"));
        }
        if (get_cookie($this->config->item("cookie_remember_code_name", "jd_auth_config"))) {
            delete_cookie($this->config->item("cookie_remember_code_name", "jd_auth_config"));
        }

        echo "Logged out! If 'remember me' was checked, those cookies were cleared. <a href='".base_url()."index.php/login'>Log in</a>";
    }

    public function user($id = NULL) {
        $user_id = isset($id) ? $id : $this->session->userdata("user_id");
        return $this->auth_model->user($user_id);
    }

    public function logged_in() {
        return (bool) $this->session->userdata("username");
    }

    public function is_admin() {
        return $this->is_in_group($this->config->item("admin_group", "jd_auth_config"));
    }

    public function is_in_group($str) {
        $user_id = $this->session->userdata("user_id");
        return $this->auth_model->is_in_group($str, $user_id);
    }

}

?>