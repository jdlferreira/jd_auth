<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Auth_model extends CI_Model {

    //FACEBOOK

    public function is_member_fb($facebook_user) {

        $this->db->where("email", $facebook_user["email"]);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function login_fb($facebook_user) {

        $this->db->where("email", $facebook_user["email"]);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));
        $user = $query->row();

        $session_data = array(
            "username" => $facebook_user["first_name"] . " " . $facebook_user["last_name"],
            "email" => $facebook_user["email"],
            "fb_login" => 1,
            "is_active" => $user->active,
            "user_id" => $user->id,
            "old_last_login" => $user->last_login
        );

        $this->update_last_login($user->id);

        //facebook users get a "remember me" cookie
        $col = $this->config->item("cookie_id_column", "jd_auth_config");
        $id_col = $user->$col;
        $hash = str_shuffle(sha1(time()));

        $this->set_user_cookies($id_col, $hash);

        $data = array(
            "remember_code" => $hash
        );

        $this->db->where("id", $user->id);
        $this->db->update("users", $data);

        $this->session->set_userdata($session_data);
    }

    public function signup_from_facebook($facebook_user) {
        $data = array(
            "email" => $facebook_user["email"],
            "username" => $facebook_user["first_name"] . " " . $facebook_user["last_name"],
            "last_login" => time(),
            "active" => 1,
            "fb_login" => 1
        );

        $this->db->insert($this->config->item("users_table", "jd_auth_config"), $data);
    }

    //END FACEBOOK

    public function check_login($username, $password) {
        $this->db->where($this->config->item("login_credential", "jd_auth_config"), $username);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));
        $row = $query->row();

        if ($query->num_rows == 1) {
            $hashed = $row->password;
            if ($this->phpass->check($password, $hashed)) {
                return $row;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function create_user($user) {
        $this->db->insert($this->config->item("users_table", "jd_auth_config"), $user);
        $user_id = $this->db->insert_id();

        $this->db->select("id");
        $this->db->where("short_desc", $this->config->item("default_group", "jd_auth_config"));
        $user_group = $this->db->get($this->config->item("user_groups_table", "jd_auth_config"))->row();

        $data = array(
            "user_id" => $user_id,
            "group_id" => $user_group->id
        );

        $this->db->insert($this->config->item("users_groups_table", "jd_auth_config"), $data);

        return $user_id;
    }

    public function remember_me($user) {
        $col = $this->config->item("cookie_id_column", "jd_auth_config");
        $id_col = $user->$col;
        $password = $user->password;
        $hash = str_shuffle(sha1($password));

        $data = array(
            "remember_code" => sha1($hash) //rehash for database security
        );

        $this->db->where("id", $user->id);
        $this->db->update("users", $data);

        $this->set_user_cookies($id_col, $hash);
    }

    public function login_remembered() {
        $cookie_id_column = $this->config->item("cookie_id_column", "jd_auth_config");
        $cookie_id_name = $this->config->item("cookie_id_name", "jd_auth_config");
        $cookie_remember_code_name = $this->config->item("cookie_remember_code_name", "jd_auth_config");

        $this->db->where($cookie_id_column, get_cookie($cookie_id_name));
        $this->db->where("remember_code", sha1(get_cookie($cookie_remember_code_name)));
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));

        if ($query->num_rows() == 1) {
            $user = $query->row();

            $this->update_last_login($user->id);

            $this->set_user_session($user);

            //update remember me hash for more security
            $this->remember_me($user);
            return true;
        } else {
            return false;
        }
    }

    public function set_lost_password_hash($email) {
        $this->db->select("id, username, password");
        $this->db->where("email", $email);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));
        $user = $query->row();

        $hash = str_shuffle(sha1($user->password));

        $data = array(
            "lost_password_key" => $hash,
        );

        $this->db->where("id", $user->id);
        $this->db->update($this->config->item("users_table", "jd_auth_config"), $data);

        $return_array = array(
            "user_id" => $user->id,
            "username" => $user->username,
            "lost_password_code" => $hash
        );

        return $return_array;
    }

    public function check_password_reset_hash($id, $str) {
        $this->db->select("id, lost_password_key");
        $this->db->where("id", $id);
        $this->db->where("lost_password_key", $str);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function change_password($user_id, $password) {
        $data = array(
            "password" => $password,
            "lost_password_key" => ""
        );

        $this->db->where("id", $user_id);
        $this->db->update($this->config->item("users_table", "jd_auth_config"), $data);
    }

    public function set_user_session($user) {
        $session_data = array(
            "username" => $user->username,
            "email" => $user->email,
            "fb_login" => 0,
            "is_active" => $user->active,
            "user_id" => $user->id,
            "old_last_login" => $user->last_login
        );

        $this->session->set_userdata($session_data);
    }

    public function set_user_cookies($id_col, $hash) {
        $cookie_id_name = $this->config->item("cookie_id_name", "jd_auth_config");
        $cookie_remember_code_name = $this->config->item("cookie_remember_code_name", "jd_auth_config");

        $expire = (60 * 60 * 24 * 365 * 2); //2 years
        set_cookie(array(
            "name" => $cookie_id_name,
            "value" => $id_col,
            "expire" => $expire
        ));

        set_cookie(array(
            "name" => $cookie_remember_code_name,
            "value" => $hash,
            "expire" => $expire
        ));
    }

    public function activate($id, $str) {
        $this->db->where("id", $id);
        $this->db->where("register_key", $str);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));

        if ($query->num_rows() == 1) {
            $data = array(
                "active" => 1,
                "register_key" => ""
            );

            $this->db->where("id", $id);
            $this->db->update($this->config->item("users_table", "jd_auth_config"), $data);

            return true;
        } else {
            return false;
        }
    }

    public function is_in_group($str, $user_id) {
        $this->db->select("id");
        $this->db->where("short_desc", $str);
        $query = $this->db->get($this->config->item("user_groups_table", "jd_auth_config"));
        $group = $query->row();

        if ($query->num_rows() == 1) {
            $this->db->where("user_id", $user_id);
            $this->db->where("group_id", $group->id);
            if ($this->db->get($this->config->item("users_groups_table", "jd_auth_config"))->num_rows() == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function user($id) {
        $this->db->where("id", $id);
        $query = $this->db->get($this->config->item("users_table", "jd_auth_config"));
        if ($query->num_rows() == 1) {
            return $query->row();
        }else{
            return false;
        }
    }

    function get_user($username) {
        $query = $this->db->get_where("users", array("username" => $username));
        return $query->row();
    }

    function username_check($str) {
        $this->db->where("username", $str);
        $query = $this->db->get("users");

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function email_check($str) {
        $this->db->where("email", $str);
        $query = $this->db->get("users");

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function update_last_login($id) {
        $data = array(
            "last_login" => time()
        );

        $this->db->where("id", $id);
        $this->db->update("users", $data);
    }

}

?>
