<script type="text/javascript" src="<?php echo base_url() ?>js/oauthpopup.js"></script>
<script type="text/javascript">   
    $(document).ready(function() {     
        $("#facebook_login").click(function(e){
            $.oauthpopup({
                path: "<?php echo base_url() ?>index.php/login/facebook_login",
                width:600,
                height:300,
                callback: function(){
                    window.location ="<?php echo base_url() ?>";
                }
            });
            e.preventDefault();
        });
    });
</script>
    <?php
    $username = array(
        "name" => "username",
        "id" => "username",
    );
    $password = array(
        "name" => "password",
        "id" => "password",
    );
    
    echo form_open("login"); ?>
    <table>
        <tr>
            <td>
                Username
            </td>
            <td>
                <?php echo form_input($username); ?>  
            </td>
        </tr>
        <tr>
            <td>
                Password
            </td>
            <td>
                <?php echo form_password($password); ?>
                <p>
                    <a href="<?php echo base_url() ?>index.php/lost_password">I forgot my password!</a>
                </p>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <?php echo form_checkbox("remember_me", "Remember Me", TRUE) . "Remember Me?"; ?>
            </td>
        </tr>

    </table>
    <?php
    echo validation_errors("<p class='form_error'>", "</p>");
    if (isset($credential_error)) {
        echo '<p class="form_error">' . $credential_error . "</p>";
    }
    if ($this->session->flashdata("message")) {
        echo '<p class="form_message">' . $this->session->flashdata("message") . '</p>';
    }
    ?>
    <?php echo form_submit("login", "Login"); ?>
    <?php echo form_close(); ?>
    <p>or...</p>
    <p><div id="facebook_login">Click here to login with Facebook</div></p>
    <p>or...</p>
    <p><a href="<?php echo base_url() ?>index.php/register">Register</a></p>