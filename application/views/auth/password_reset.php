<?php echo form_open("password_reset/".$user_id . "/" . $hash); ?>
<table>
    <tr>
        <td>
            Password
        </td>
        <td>
            <?php echo form_input($password); ?>  
        </td>
    </tr>
    <tr>
        <td>
            Confirm Password
        </td>
        <td>
            <?php echo form_password($password_confirm); ?>
        </td>
    </tr>
</table>
<?php echo validation_errors("<p class='form_error'>", "</p>"); ?>
<?php echo form_submit('reset_password', 'Reset Password'); ?>
<?php echo form_close(); ?>
</div>