<script type="text/javascript" src="<?php echo base_url() ?>js/oauthpopup.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#facebook_login").click(function(e){
            $.oauthpopup({
                path: '<?php echo base_url() ?>index.php/login/facebook_login',
                width:600,
                height:300,
                callback: function(){
                    window.location ="<?php echo base_url() ?>";
                }
            });
            e.preventDefault();
        });
    });      
</script>
<?php echo form_open("register"); ?>
<table>
<tr>
    <td>
        Username
    </td>
    <td>
        <?php echo form_input($username_tooltip);?>  
    </td>
</tr>
<tr>
    <td>
        Password
    </td>
    <td>
        <?php echo form_password($password_tooltip); ?>  
    </td>
</tr>
<tr>
    <td>
        Confirm Password
    </td>
    <td>
        <?php echo form_password($password_conf); ?>  
    </td>
</tr>
<tr>
    <td>
        Email
    </td>
    <td>
        <?php echo form_input($email_tooltip); ?>  
    </td>     
</tr>
</table>
<?php echo validation_errors("<p class='form_error'>", "</p>") ?>
<?php echo form_submit('register', 'Register'); ?>
<?php echo form_close(); ?>
<p>or...</p>
<p><div id="facebook_login">Click here to login with Facebook</div></p>
