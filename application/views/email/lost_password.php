<html>
<body>
	<h1><?php echo $title ?> - Password Reset!</h1>
        <p><?php echo $username ?>, to reset your password you must click the following link: <a href="<?php echo base_url() ?>index.php/password_reset/<?php echo $user_id ?>/<?php echo $lost_password_code; ?>">Click here!</a></p>
</body>
</html>