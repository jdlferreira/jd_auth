<html>
<body>
	<h1>Activate your <?php echo $title ?> account!</h1>
        <p>Thank you for your registration, <?php echo $username ?>. In order to activate your account you must click the following link: <a href="<?php echo base_url() ?>index.php/activate/<?php echo $user_id ?>/<?php echo $register_key; ?>">Click here!</a></p>
</body>
</html>